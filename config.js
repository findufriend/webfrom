/**
 * Created by Foold on 2014-5-18.
 */
exports.allowed=function(){

    return [
        /^\/login/,
        /^\/signin/,
        /^\/logout/,
        /^\/child/,
        /^\/mainmenu/,
        /^\/images/,
        /^\/image/,
        /jsonall$/,
        /^\/detail/

    ];
};

exports.mongoconfig = function(){

    var opts = {
        "database": "west2",
        "host": "localhost",
        "port": 27017
    };

    opts['url'] = "mongodb://"+opts.host+":"+opts.port+"/"+opts.database;


    return opts;

}

