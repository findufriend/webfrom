/**
 * Created by Foold on 2014-4-28.
 */
$(document).ready(function() {
     buildZtree('#jMenu');

});

function buildZtree(selector){
    $.get('/mainmenu/onlyone',function(menus){
        var items = $(menus.content);
        var children = items.children("li");
        children.sort(function(item1,item2){
            var s1 =  $(item1).attr('sort');
            var s2 =  $(item2).attr('sort');
            if(s1<s2) return -1;
            if(s1==s2) return 0;
            return 1;
        });
        var jmenu = $(selector);
        jmenu.append(children);
        //log(jmenu[0].outerHTML);
        initJmenu($(selector));
    });
}

function initJmenu(jqObj){
    jqObj.jMenu({
        openClick : false,
        ulWidth :'auto',
        absoluteTop:50,
        TimeBeforeOpening : 100,
        TimeBeforeClosing : 11,
        animatedText : false,
        paddingLeft: 1,
        effects : {
            effectSpeedOpen : 150,
            effectSpeedClose : 150,
            effectTypeOpen : 'slide',
            effectTypeClose : 'slide',
            effectOpen : 'swing',
            effectClose : 'swing'
        }

    });

}