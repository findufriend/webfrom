/**
 * The abbr dialog definition.
 *
 * Created out of the CKEditor Plugin SDK:
 * http://docs.ckeditor.com/#!/guide/plugin_sdk_sample_1
 */

function mediaPlay(url){
    var code = [
            '<object id="test" width="500" height="300"   classid="CLSID:6BF52A52-394A-11d3-B153-00C04F79FAA6">'
        ,'<param name="url" value="'+url+'" >'
        ,'<param name="currentPosition" value="0">'
        ,'     <param name="ShowDisplay" value="1">'
        ,'          <param name="AutoSize" value="0">'
        ,'              <param name="AutoStart" value="0">'
        ,'                  <param name="ShowCaptioning" value="1">'
        ,'                      <embed'
        ,'                      type="application/x-mplayer2"'
        ,'                      pluginspage="http://www.microsoft.com/Windows/Downloads/Contents/Products/MediaPlayer/"'
        ,'                      Name="wmp"'
        ,'                      id="wmp"'
        ,'                      src="'+url+'"'
        ,'                      HEIGHT="300"'
        ,'                      WIDTH="500"'
        ,'                      ShowControls="1"'
        ,'                      ShowPositionControls="0"'
        ,'                      ShowAudioControls="1"'
        ,'                      ShowTracker="0"'
        ,'                      ShowDisplay="0"'
        ,'                      ShowStatusBar="1"'
        ,'                      ShowGoToBar="0"'
        ,'                      ShowCaptioning="0"'
        ,'                      AutoStart="0"'
        ,'                      AutoRewind="0"'
        ,'                      AnimationAtStart="0"'
        ,'                      TransparentAtStart="0"'
        ,'                      AllowChangeDisplaySize="0"'
        ,'                      AllowScan="0"'
        ,'                      EnableContextMenu="0"'
        ,'                      ClickToPlay="0"></embed>'
        ,'                  </object>'];

    return code.join('');
}


// Our dialog definition.
CKEDITOR.dialog.add( 'abbrDialog', function( editor ) {
	return {

		// Basic properties of the dialog window: title, minimum size.
		title: '插入视频地址',
		minWidth: 400,
		minHeight: 200,

		// Dialog window contents definition.
		contents: [
			{
				// Definition of the Basic Settings dialog tab (page).
				id: 'tab-basic',

				// The tab contents.
				elements: [
					{
						// Text input field for the abbreviation text.
						type: 'text',
						id: 'abbr',
						label: '视频地址',

						// Validation checking whether the field is not empty.
						validate: CKEDITOR.dialog.validate.notEmpty( "视频地址不能为空" ),

						// Called by the main setupContent call on dialog initialization.
						setup: function( element ) {
							///log(this);
							//this.setValue( element.getText() );
						},

						// Called by the main commitContent call on dialog confirmation.
						commit: function( element ) {
                            //log(this);
							//element.setText( this.getValue() );
                            //editor.insertElement("<abbr>sdfsdf</abbr>");
						}
					}
				]
			}
		],

		// Invoked when the dialog is loaded.
		onShow: function() {
			this.setupContent( this.element );
		},

		// This method is invoked once a user clicks the OK button, confirming the dialog.
		onOk: function() {
            var dialog = this;
            var url = dialog.getValueOf( 'tab-basic', 'abbr' );
            var code = mediaPlay(url);
            var abbr = editor.document.createElement( 'div' );
            abbr.appendHtml(code);
            editor.insertElement( abbr );

        }
	};
});