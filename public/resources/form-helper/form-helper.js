/**
 * Created by Foold on 14-2-17.
 */

//
// create closure
//
(function ($) {

    var helper = {

    text:function (exn, bean,kval) {
        exn.value = kval;
    },
    password:function (exn, bean,kval) {
        this.text(exn, bean,kval);
    },
    hidden:function(exn,bean,kval){
        this.text(exn, bean,kval);
    } ,
    textarea : function (exn, bean,kval) {
        exn.innerText = kval;
        if(CKEDITOR.instances[exn.name]){
            CKEDITOR.instances[exn.name].setData(kval);
        }

    },
    checkbox :function (exn, bean,kval) {


        //log(exn.value);
        //log(kval);
        if(!$.isArray(exn.value)){
            exn.checked = 'checked';
            return;
        }

        if (kval && ($.inArray(exn.value,kval)!=-1)) {
            exn.checked = 'checked';
            return;
        }
    },
    radio : function (exn, bean,kval) {
        this.checkbox(exn, bean,kval);
    },
    selectone :function (exn, bean,kval) {
        var opts = exn.childNodes;
        var ntxt = $(exn).next('input:hidden');
        for (var n = 0; n < opts.length; n++) {
            var opt = opts[n];
            if (opt.value == kval) {
                opt.selected = 'selected';

                if(ntxt){
                    ntxt.val(opt.innerText);
                }
                break;
            }
        }

        $(exn).bind('change',function(event){
            var setd = $("option:selected",exn);
            if(setd){
                ntxt.val(setd.text());
            }
        });





    },
    selectmultiple : function (exn, bean,kval) {
        var opts = exn.childNodes;
        for (var n = 0; n < opts.length; n++) {
            var opt = opts[n];
            if ($.inArray(opt.value,kval)!=-1) {
                opt.selected = 'selected';
            }
        }
    },
    _convert : function (exn, bean,kval) {

        var mtype = exn.type.replace(/-/,"");
        //console.log(exn.type +"=>"+mtype);
        this[mtype].call(this,exn,bean,kval);

    },
    _transExpress:function(expr){

        var ptn= /\[(\d+)\]/g;
        if(ptn.test(expr)){
            return expr.replace(ptn,"");
        }
        var ptn2 = /\[([a-z]+)\]/g;
        var exp = expr.replace(ptn2,".$1");
        return exp;
    },
    _parseValue:function(bean,aname){
        var vexpr = this._transExpress(aname);
        if(!vexpr) return null;
        var kval = null;
        try{
            var key = "bean."+vexpr;
            kval = eval(key);
            if(!kval) return null;
        }catch(exception) {
            return null;
        }

        return kval;
    },
    helpform : function (opts, formc) {
        //var x = document.getElementById(formId);
        var bean = opts['beans'];
        var exlds = ['submit','reset','button','image','file'];
        for (var i = 0; i < formc.length; i++) {
            var exn = formc.elements[i];
            var etype = exn.type;
            //alert(exlds);
            //alert(etype);
            if ($.inArray(etype,exlds)!=-1)continue;
            var ename = exn.name;
            if (!ename || !etype) continue;

            var kval = this._parseValue(bean,ename);
            if (kval)this._convert(exn,bean,kval);

        }
        //针对imagelist特殊处理

        var imageValues = bean[opts.images['field']];
        if(opts.images && imageValues && imageValues[0] && imageValues[0].length>=1){
            var list = typeof(imageValues[0])=='string'?imageValues:imageValues[0];
            //log(data.list);
            var html = template.render(opts.images['tempId'], {list:list});
            //log(html);
            $(opts.images['show']).append(html);
        }
    }
}

    $.fn.helpform = function (options) {
       // debug(this);
        var opts = $.extend({}, $.fn.helpform.defaults, options);

        return this.each(function () {
            $this = $(this);
            helper.helpform(opts,this);
        });
    };
//
// private function for debugging
//
    function debug($obj) {
        //if (window.console && window.console.log) window.console.dir($obj);
    };

    $.fn.helpform.defaults = {beans: []};


})(jQuery);
