/**
 * Created with JetBrains PhpStorm.
 * User: 饭
 * Date: 12-11-29
 * Time: 下午9:44
 * To change this template use File | Settings | File Templates.
 */

$(function () {


    //replaceDiv("filedata");

   // $(".container .sidebar ul.nav-sidebar li a ").bind('click',sideHandler);

   /* var path = window.location.pathname;
    $(".container .sidebar ul.nav-sidebar li").removeClass('active');
    $(".container .sidebar ul.nav-sidebar li a[href='"+path+"']").parent('li').addClass('active');*/

    $(document.body).on("click","ul.pagination li a",function(event){
        var link = $(event.target).attr('link');
        if(!link) return true;
        $("#dataTable").load(link+"?layout=0");
    });

    $(document).ajaxStart(function(){
        info("正在加载...");
    }).ajaxSuccess(function(){
        info("加载完成");
    }).ajaxError(function(event, XMLHttpRequest, ajaxOptions, thrownError){
       info('加载错误:'+thrownError);
    });


});
var editor;

function replaceDiv( div ) {
    if ( editor )
        editor.destroy();
    var cfg =  {
        filebrowserUploadUrl :'/upload',
        filebrowserImageUploadUrl :'/upload?type=images'
    };
    editor = CKEDITOR.replace( div ,cfg);
}

function sideHandler(event){

    var url = $(event.target).attr("link");
    if(url=="#" || url==undefined){
        return;
    }
    window.location.href=url;

}
