/*
 * GET users listing.
 */

var routers = require('commons-dao')(require('path').basename(module.filename, '.js'));
var form = require('express-form');
var field = form.field;

function getRules(i18n) {

    return [
       field('sender').required("",i18n.__('required', '用户')),
       field('category').required("",i18n.__('required', '留言分类')),
       field('sendTime').required("",i18n.__('required', '留言时间')),
       field('content').required("",i18n.__('required', '留言内容')),
       field('ipaddr').required("",i18n.__('required', '留言IP地址'))
    ];

}


module.exports = function (app, i18n) {

    routers.init(app, getRules(i18n));

}







