
/*
 * GET home page.
 */

exports.admin = function(req, res){
     res.redirect("/company/show");
};

exports.login = function(req,res){
    res.render('login',{'message':'',layout:false});
}

exports.logout = function(req,res){
    req.session.destroy(function(err){
       if(err) {
           console.log(err);
           throw new Error(err);
       }

    });
    res.render('login',{'message':'',layout:false});
}