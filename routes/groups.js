/*
 * GET users listing.
 */

var routers = require('commons-dao')(require('path').basename(module.filename,'.js'));
var form = require('express-form');
var field = form.field;

function getRules(i18n){

    return [
        field("name").trim().required("",i18n.__('required','名称')),
        field('webrests').array().required()
        //,field("remark").trim().required("",i18n.__('required','备注'))
    ];

}


module.exports = function(app,i18n){

    routers.init(app,getRules(i18n));

}







