/*
 * GET users listing.
 */

var routers = require('commons-dao')(require('path').basename(module.filename,'.js'));
var form = require('express-form');
var field = form.field;
function findCompany(req, res, db, col, query){
    col.findOne(query,function(err, doc) {
        //console.log(doc);

        res.render('company/show', doc);
        db.close();

    });
}
function getRules(i18n){

    return [
        field("name").trim().required("",i18n.__('required','名称')),
        field("address").trim().required("", i18n.__('required','密码')),
        field("website").trim().required("",i18n.__('numbered','年龄')),
        field('phone').trim().required("",i18n.__('required','联系电话')),
        field('email').trim().required("",i18n.__('required','邮件地址')).isEmail(),
        field('postcode').trim().required("",i18n.__('required','邮编')),
        field('maindesc').trim().required("",i18n.__('required','简介')),
        field('weichat').trim().required("",i18n.__('required','微信账号'))
    ];

}


module.exports = function(app,i18n){

    app.get('/company/show',function(req,res){
        routers.doDbOperation(req, res, { "_id": { $exists: true }}, findCompany);
    });
    routers.init(app,getRules(i18n));


}







