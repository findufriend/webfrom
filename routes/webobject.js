/*
 * GET users listing.
 */

var routers = require('commons-dao')(require('path').basename(module.filename,'.js'));
var form = require('express-form');
var field = form.field;
var webrest = require('commons-dao')('webrest');
var utils = require('my-utils');
function getRules(i18n){

    return [
        field("name").trim().required("",i18n.__('required','名称')),
        field('webprefix').trim().required("", i18n.__('required','URL前缀'))
    ];

}

function externaldb(collection,db,vbean){
   // console.dir(vbean);
    collection.insert(vbean, {w:1}, function(err, result) {
        if(err){
            console.log(err);
        }

        db.close();
    });
}

function middlecall(req,res,next){

    var id =  req.params.id;
    if(id==routers.defaultId){

        var name = req.body.name;
        var webprefix = req.body.webprefix;
        var curd = [
            {'webpath':'/upsert','name':'更新','common':false,'mtype':0}
            ,{'webpath':'/list','name':'列表浏览','common':false,'mtype':0}
            ,{'webpath':'/remove','name':'删除','common':false,'mtype':0}
            ,{'webpath':'/findone','name':'查看','common':false,'mtype':0}
            ,{'webpath':'/jsonall','name':'JSON数据','common':true,'mtype':0}
            ,{'webpath':'','name':'添加','common':false,'mtype':0}
        ];

        var vbean = [];
        for(var idx in curd){
            var bean = curd[idx];
            bean.name = name+bean.name;
            bean.webpath = "/"+webprefix+bean.webpath;
            vbean.push(bean);

        }


        webrest.doMiddleCrud(vbean,externaldb);
    }
    next();
}

module.exports = function(app,i18n){

    routers.init(app,getRules(i18n),[middlecall]);

}







