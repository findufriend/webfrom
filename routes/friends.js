/**
 * Created by Foold on 14-2-15.
 */

/*
 * GET users listing.
 */
var cname  = require('path').basename(module.filename,'.js');
var routers = require('commons-dao')(cname);
var form = require('express-form');
var field = form.field;

function getRules(i18n){


    return [
       field('name').trim().required("", i18n.__('required','名称')),
       field('link').trim().required("", i18n.__('required','连接')).isUrl("必须是一个网址")
    ];

}

module.exports = function(app,i18n){
    routers.init(app,getRules(i18n));
}