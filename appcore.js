
/**
 * Module dependencies.
 */

var express = require('express');
var i18n    = require("i18n");
var routes  = require('./routes');

var http = require('http');
var path = require('path');
var fs = require('fs');
var partials = require('express-partials');
var helpers = require('express-helpers');
var fhelper = require('form-converter');
var app = express();
var MongoStore = require('connect-mongo')(express);
var secContext = require('local-security')();
app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');


app.use(express.favicon());
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.methodOverride());
app.use(express.cookieParser());
app.use(express.bodyParser({uploadDir:'public/images'}));
//app.use(express.cookieSession({key:'west2.sess',secret :"west2"}));
app.use(express.session({
    secret: "west2",
    key: 'west2.sess',
    cookie: {maxAge: 24*3600000},
    store: new MongoStore({
        db: "west2"
    })
}));
app.use(partials());
helpers(app);
fhelper(app);
// development only
if ('development' == app.get('env')) {
    app.use(express.errorHandler());
}

app.use(secContext.authentication());

i18n.configure({
    locales:['zh', 'en'],
    directory: __dirname + '/public/locales',
    defaultLocale: 'zh',
    cookie: 'wlocal',
    updateFiles: false,
    indent: "\t",
    // setting extension of json files - defaults to '.json' (you might want to set this to '.js' according to webtranslateit)
    extension: '.json'
});
// default: using 'accept-language' header to guess language settings
app.use(i18n.init);
app.get('/login', routes.login);
app.get('/logout', routes.logout);
app.post('/signin',secContext.authorize);


app.use(app.router);

/**延迟注册控制器**/
var modules= [
    'worklog',
    'users',
    'article',
    'articlec',
    'vmessage',
    'friends',
    'groups',
    'webrest',
    'company',
    'child',
    'webobject'

];
for(var m in modules){
    var mk= modules[m];
    require('./routes/'+mk)(app,i18n);
}


/**控制器注册完成**/
app.get('/admin',routes.admin);

app.post('/images/upload', function(req, res) {
    /* // 获得文件的临时路径
     var tmp_path = req.files.thumbnail.path;

     // 指定文件上传后的目录 - 示例为"images"目录。
     var target_path = './public/images/' + req.files.thumbnail.name;
     // 移动文件
     fs.rename(tmp_path, target_path, function(err) {
     if (err) throw err;
     // 删除临时文件夹文件,
     fs.unlink(tmp_path, function() {
     if (err) throw err;
     res.send('File uploaded to: ' + target_path + ' - ' + req.files.thumbnail.size + ' bytes');
     });
     });*/
    var num = req.query.CKEditorFuncNum;
    var ppath = req.files.upload.path;
    console.log(ppath);
    ppath = ppath.replace('public','');
    ppath = ppath.replace(/\\/g,'/');

    //console.log(ppath);
    res.send("<script>window.parent.CKEDITOR.tools.callFunction("
        + num +
        ",'" +ppath+ "'"+",'');</script>");
});

app.post('/images/json', function(req, res) {


    var ppath = req.files.files.path;
    // console.log(req.files);
    ppath = ppath.replace('public','');
    ppath = ppath.replace(/\\/g,'/');

    console.log(ppath);
    var result = {'path':ppath};
    res.send(JSON.stringify(result));
});

app.get('/dwz', function(req, res) {
    res.render("dwz",{layout:false});
});

exports.init = function(port){
    http.createServer(app).listen(port, function(){
          console.log('Express server listening on port ' +  port);
    });
}